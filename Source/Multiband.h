/* ------------------------------------------------------------
name: "multiband"
Code generated with Faust 2.38.0 (https://faust.grame.fr)
Compilation options: -a faustMinimal.h -lang cpp -es 1 -single -ftz 0
------------------------------------------------------------ */

#ifndef  __Multiband_H__
#define  __Multiband_H__

#include <cmath>
#include <cstring>

/************************** BEGIN MapUI.h **************************/
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef FAUST_MAPUI_H
#define FAUST_MAPUI_H

#include <vector>
#include <map>
#include <string>
#include <stdio.h>

/************************** BEGIN UI.h **************************/
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2020 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __UI_H__
#define __UI_H__

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

/*******************************************************************************
 * UI : Faust DSP User Interface
 * User Interface as expected by the buildUserInterface() method of a DSP.
 * This abstract class contains only the method that the Faust compiler can
 * generate to describe a DSP user interface.
 ******************************************************************************/

struct Soundfile;

template <typename REAL>
struct UIReal
{
    UIReal() {}
    virtual ~UIReal() {}
    
    // -- widget's layouts
    
    virtual void openTabBox(const char* label) = 0;
    virtual void openHorizontalBox(const char* label) = 0;
    virtual void openVerticalBox(const char* label) = 0;
    virtual void closeBox() = 0;
    
    // -- active widgets
    
    virtual void addButton(const char* label, REAL* zone) = 0;
    virtual void addCheckButton(const char* label, REAL* zone) = 0;
    virtual void addVerticalSlider(const char* label, REAL* zone, REAL init, REAL min, REAL max, REAL step) = 0;
    virtual void addHorizontalSlider(const char* label, REAL* zone, REAL init, REAL min, REAL max, REAL step) = 0;
    virtual void addNumEntry(const char* label, REAL* zone, REAL init, REAL min, REAL max, REAL step) = 0;
    
    // -- passive widgets
    
    virtual void addHorizontalBargraph(const char* label, REAL* zone, REAL min, REAL max) = 0;
    virtual void addVerticalBargraph(const char* label, REAL* zone, REAL min, REAL max) = 0;
    
    // -- soundfiles
    
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) = 0;
    
    // -- metadata declarations
    
    virtual void declare(REAL* zone, const char* key, const char* val) {}
    
    // To be used by LLVM client
    virtual int sizeOfFAUSTFLOAT() { return sizeof(FAUSTFLOAT); }
};

struct UI : public UIReal<FAUSTFLOAT>
{
    UI() {}
    virtual ~UI() {}
};

#endif
/**************************  END  UI.h **************************/
/************************** BEGIN PathBuilder.h **************************/
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef FAUST_PATHBUILDER_H
#define FAUST_PATHBUILDER_H

#include <vector>
#include <string>
#include <algorithm>

/*******************************************************************************
 * PathBuilder : Faust User Interface
 * Helper class to build complete hierarchical path for UI items.
 ******************************************************************************/

class PathBuilder
{

    protected:
    
        std::vector<std::string> fControlsLevel;
       
    public:
    
        PathBuilder() {}
        virtual ~PathBuilder() {}
    
        std::string replaceCharList(std::string str, const std::vector<char>& ch1, char ch2)
        {
            std::vector<char>::const_iterator beg = ch1.begin();
            std::vector<char>::const_iterator end = ch1.end();
            for (size_t i = 0; i < str.length(); ++i) {
                if (std::find(beg, end, str[i]) != end) {
                    str[i] = ch2;
                }
            }
            return str;
        }
    
        std::string buildPath(const std::string& label) 
        {
            std::string res = "/";
            for (size_t i = 0; i < fControlsLevel.size(); i++) {
                res += fControlsLevel[i];
                res += "/";
            }
            res += label;
            std::vector<char> rep = {' ', '#', '*', ',', '/', '?', '[', ']', '{', '}', '(', ')'};
            replaceCharList(res, rep, '_');
            return res;
        }
    
        void pushLabel(const std::string& label) { fControlsLevel.push_back(label); }
        void popLabel() { fControlsLevel.pop_back(); }
    
};

#endif  // FAUST_PATHBUILDER_H
/**************************  END  PathBuilder.h **************************/

/*******************************************************************************
 * MapUI : Faust User Interface
 * This class creates a map of complete hierarchical path and zones for each UI items.
 ******************************************************************************/

class MapUI : public UI, public PathBuilder
{
    
    protected:
    
        // Complete path map
        std::map<std::string, FAUSTFLOAT*> fPathZoneMap;
    
        // Label zone map
        std::map<std::string, FAUSTFLOAT*> fLabelZoneMap;
    
    public:
        
        MapUI() {}
        virtual ~MapUI() {}
        
        // -- widget's layouts
        void openTabBox(const char* label)
        {
            pushLabel(label);
        }
        void openHorizontalBox(const char* label)
        {
            pushLabel(label);
        }
        void openVerticalBox(const char* label)
        {
            pushLabel(label);
        }
        void closeBox()
        {
            popLabel();
        }
        
        // -- active widgets
        void addButton(const char* label, FAUSTFLOAT* zone)
        {
            fPathZoneMap[buildPath(label)] = zone;
            fLabelZoneMap[label] = zone;
        }
        void addCheckButton(const char* label, FAUSTFLOAT* zone)
        {
            fPathZoneMap[buildPath(label)] = zone;
            fLabelZoneMap[label] = zone;
        }
        void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT fmin, FAUSTFLOAT fmax, FAUSTFLOAT step)
        {
            fPathZoneMap[buildPath(label)] = zone;
            fLabelZoneMap[label] = zone;
        }
        void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT fmin, FAUSTFLOAT fmax, FAUSTFLOAT step)
        {
            fPathZoneMap[buildPath(label)] = zone;
            fLabelZoneMap[label] = zone;
        }
        void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT fmin, FAUSTFLOAT fmax, FAUSTFLOAT step)
        {
            fPathZoneMap[buildPath(label)] = zone;
            fLabelZoneMap[label] = zone;
        }
        
        // -- passive widgets
        void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT fmin, FAUSTFLOAT fmax)
        {
            fPathZoneMap[buildPath(label)] = zone;
            fLabelZoneMap[label] = zone;
        }
        void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT fmin, FAUSTFLOAT fmax)
        {
            fPathZoneMap[buildPath(label)] = zone;
            fLabelZoneMap[label] = zone;
        }
    
        // -- soundfiles
        virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}
        
        // -- metadata declarations
        virtual void declare(FAUSTFLOAT* zone, const char* key, const char* val)
        {}
        
        // setParamValue/getParamValue
        void setParamValue(const std::string& path, FAUSTFLOAT value)
        {
            if (fPathZoneMap.find(path) != fPathZoneMap.end()) {
                *fPathZoneMap[path] = value;
            } else if (fLabelZoneMap.find(path) != fLabelZoneMap.end()) {
                *fLabelZoneMap[path] = value;
            } else {
                fprintf(stderr, "ERROR : setParamValue '%s' not found\n", path.c_str());
            }
        }
        
        FAUSTFLOAT getParamValue(const std::string& path)
        {
            if (fPathZoneMap.find(path) != fPathZoneMap.end()) {
                return *fPathZoneMap[path];
            } else if (fLabelZoneMap.find(path) != fLabelZoneMap.end()) {
                return *fLabelZoneMap[path];
            } else {
                fprintf(stderr, "ERROR : getParamValue '%s' not found\n", path.c_str());
                return 0;
            }
        }
    
        // map access 
        std::map<std::string, FAUSTFLOAT*>& getMap() { return fPathZoneMap; }
        
        int getParamsCount() { return int(fPathZoneMap.size()); }
        
        std::string getParamAddress(int index)
        {
            if (index < 0 || index > int(fPathZoneMap.size())) {
                return "";
            } else {
                auto it = fPathZoneMap.begin();
                while (index-- > 0 && it++ != fPathZoneMap.end()) {}
                return it->first;
            }
        }
        
        const char* getParamAddress1(int index)
        {
            if (index < 0 || index > int(fPathZoneMap.size())) {
                return nullptr;
            } else {
                auto it = fPathZoneMap.begin();
                while (index-- > 0 && it++ != fPathZoneMap.end()) {}
                return it->first.c_str();
            }
        }
    
        std::string getParamAddress(FAUSTFLOAT* zone)
        {
            for (const auto& it : fPathZoneMap) {
                if (it.second == zone) return it.first;
            }
            return "";
        }
    
        FAUSTFLOAT* getParamZone(const std::string& str)
        {
            if (fPathZoneMap.find(str) != fPathZoneMap.end()) {
                return fPathZoneMap[str];
            } else if (fLabelZoneMap.find(str) != fLabelZoneMap.end()) {
                return fLabelZoneMap[str];
            }
            return nullptr;
        }
    
        FAUSTFLOAT* getParamZone(int index)
        {
            if (index < 0 || index > int(fPathZoneMap.size())) {
                return nullptr;
            } else {
                auto it = fPathZoneMap.begin();
                while (index-- > 0 && it++ != fPathZoneMap.end()) {}
                return it->second;
            }
        }
    
        static bool endsWith(const std::string& str, const std::string& end)
        {
            size_t l1 = str.length();
            size_t l2 = end.length();
            return (l1 >= l2) && (0 == str.compare(l1 - l2, l2, end));
        }
};


#endif // FAUST_MAPUI_H
/**************************  END  MapUI.h **************************/
/************************** BEGIN meta.h **************************/
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __meta__
#define __meta__

/**
 The base class of Meta handler to be used in dsp::metadata(Meta* m) method to retrieve (key, value) metadata.
 */
struct Meta
{
    virtual ~Meta() {};
    virtual void declare(const char* key, const char* value) = 0;
};

#endif
/**************************  END  meta.h **************************/
/************************** BEGIN dsp.h **************************/
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __dsp__
#define __dsp__

#include <string>
#include <vector>

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

struct UI;
struct Meta;

/**
 * DSP memory manager.
 */

struct dsp_memory_manager {
    
    virtual ~dsp_memory_manager() {}
    
    virtual void* allocate(size_t size) = 0;
    virtual void destroy(void* ptr) = 0;
    
};

/**
* Signal processor definition.
*/

class dsp {

    public:

        dsp() {}
        virtual ~dsp() {}

        /* Return instance number of audio inputs */
        virtual int getNumInputs() = 0;
    
        /* Return instance number of audio outputs */
        virtual int getNumOutputs() = 0;
    
        /**
         * Trigger the ui_interface parameter with instance specific calls
         * to 'openTabBox', 'addButton', 'addVerticalSlider'... in order to build the UI.
         *
         * @param ui_interface - the user interface builder
         */
        virtual void buildUserInterface(UI* ui_interface) = 0;
    
        /* Return the sample rate currently used by the instance */
        virtual int getSampleRate() = 0;
    
        /**
         * Global init, calls the following methods:
         * - static class 'classInit': static tables initialization
         * - 'instanceInit': constants and instance state initialization
         *
         * @param sample_rate - the sampling rate in Hz
         */
        virtual void init(int sample_rate) = 0;

        /**
         * Init instance state
         *
         * @param sample_rate - the sampling rate in Hz
         */
        virtual void instanceInit(int sample_rate) = 0;
    
        /**
         * Init instance constant state
         *
         * @param sample_rate - the sampling rate in Hz
         */
        virtual void instanceConstants(int sample_rate) = 0;
    
        /* Init default control parameters values */
        virtual void instanceResetUserInterface() = 0;
    
        /* Init instance state (like delay lines...) but keep the control parameter values */
        virtual void instanceClear() = 0;
 
        /**
         * Return a clone of the instance.
         *
         * @return a copy of the instance on success, otherwise a null pointer.
         */
        virtual dsp* clone() = 0;
    
        /**
         * Trigger the Meta* parameter with instance specific calls to 'declare' (key, value) metadata.
         *
         * @param m - the Meta* meta user
         */
        virtual void metadata(Meta* m) = 0;
    
        /**
         * DSP instance computation, to be called with successive in/out audio buffers.
         *
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) = 0;
    
        /**
         * DSP instance computation: alternative method to be used by subclasses.
         *
         * @param date_usec - the timestamp in microsec given by audio driver.
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (either float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (either float, double or quad)
         *
         */
        virtual void compute(double /*date_usec*/, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }
       
};

/**
 * Generic DSP decorator.
 */

class decorator_dsp : public dsp {

    protected:

        dsp* fDSP;

    public:

        decorator_dsp(dsp* dsp = nullptr):fDSP(dsp) {}
        virtual ~decorator_dsp() { delete fDSP; }

        virtual int getNumInputs() { return fDSP->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP->getNumOutputs(); }
        virtual void buildUserInterface(UI* ui_interface) { fDSP->buildUserInterface(ui_interface); }
        virtual int getSampleRate() { return fDSP->getSampleRate(); }
        virtual void init(int sample_rate) { fDSP->init(sample_rate); }
        virtual void instanceInit(int sample_rate) { fDSP->instanceInit(sample_rate); }
        virtual void instanceConstants(int sample_rate) { fDSP->instanceConstants(sample_rate); }
        virtual void instanceResetUserInterface() { fDSP->instanceResetUserInterface(); }
        virtual void instanceClear() { fDSP->instanceClear(); }
        virtual decorator_dsp* clone() { return new decorator_dsp(fDSP->clone()); }
        virtual void metadata(Meta* m) { fDSP->metadata(m); }
        // Beware: subclasses usually have to overload the two 'compute' methods
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(count, inputs, outputs); }
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(date_usec, count, inputs, outputs); }
    
};

/**
 * DSP factory class, used with LLVM and Interpreter backends
 * to create DSP instances from a compiled DSP program.
 */

class dsp_factory {
    
    protected:
    
        // So that to force sub-classes to use deleteDSPFactory(dsp_factory* factory);
        virtual ~dsp_factory() {}
    
    public:
    
        virtual std::string getName() = 0;
        virtual std::string getSHAKey() = 0;
        virtual std::string getDSPCode() = 0;
        virtual std::string getCompileOptions() = 0;
        virtual std::vector<std::string> getLibraryList() = 0;
        virtual std::vector<std::string> getIncludePathnames() = 0;
    
        virtual dsp* createDSPInstance() = 0;
    
        virtual void setMemoryManager(dsp_memory_manager* manager) = 0;
        virtual dsp_memory_manager* getMemoryManager() = 0;
    
};

// Denormal handling

#if defined (__SSE__)
#include <xmmintrin.h>
#endif

class ScopedNoDenormals
{
    private:
    
        intptr_t fpsr;
        
        void setFpStatusRegister(intptr_t fpsr_aux) noexcept
        {
        #if defined (__arm64__) || defined (__aarch64__)
           asm volatile("msr fpcr, %0" : : "ri" (fpsr_aux));
        #elif defined (__SSE__)
            _mm_setcsr(static_cast<uint32_t>(fpsr_aux));
        #endif
        }
        
        void getFpStatusRegister() noexcept
        {
        #if defined (__arm64__) || defined (__aarch64__)
            asm volatile("mrs %0, fpcr" : "=r" (fpsr));
        #elif defined ( __SSE__)
            fpsr = static_cast<intptr_t>(_mm_getcsr());
        #endif
        }
    
    public:
    
        ScopedNoDenormals() noexcept
        {
        #if defined (__arm64__) || defined (__aarch64__)
            intptr_t mask = (1 << 24 /* FZ */);
        #else
            #if defined(__SSE__)
            #if defined(__SSE2__)
                intptr_t mask = 0x8040;
            #else
                intptr_t mask = 0x8000;
            #endif
            #else
                intptr_t mask = 0x0000;
            #endif
        #endif
            getFpStatusRegister();
            setFpStatusRegister(fpsr | mask);
        }
        
        ~ScopedNoDenormals() noexcept
        {
            setFpStatusRegister(fpsr);
        }

};

#define AVOIDDENORMALS ScopedNoDenormals();

#endif

/************************** END dsp.h **************************/

// BEGIN-FAUSTDSP


#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif 

#include <algorithm>
#include <cmath>
#include <cstdint>
#include <math.h>

static float Multiband_faustpower2_f(float value) {
	return (value * value);
}

#ifndef FAUSTCLASS 
#define FAUSTCLASS Multiband
#endif

#ifdef __APPLE__ 
#define exp10f __exp10f
#define exp10 __exp10
#endif

class Multiband : public dsp {
	
 private:
	
	int fSampleRate;
	float fConst1;
	float fConst2;
	float fConst3;
	FAUSTFLOAT fVslider0;
	FAUSTFLOAT fVslider1;
	float fConst4;
	float fConst5;
	float fConst6;
	float fConst7;
	float fConst8;
	float fRec0[3];
	FAUSTFLOAT fVslider2;
	FAUSTFLOAT fVslider3;
	FAUSTFLOAT fVslider4;
	FAUSTFLOAT fVslider5;
	FAUSTFLOAT fVslider6;
	float fRec1[2];
	FAUSTFLOAT fVbargraph0;
	FAUSTFLOAT fVslider7;
	FAUSTFLOAT fVslider8;
	float fRec2[3];
	FAUSTFLOAT fVslider9;
	FAUSTFLOAT fVslider10;
	FAUSTFLOAT fVslider11;
	FAUSTFLOAT fVslider12;
	FAUSTFLOAT fVslider13;
	float fRec3[2];
	FAUSTFLOAT fVbargraph1;
	FAUSTFLOAT fVslider14;
	FAUSTFLOAT fVslider15;
	float fRec4[3];
	FAUSTFLOAT fVslider16;
	FAUSTFLOAT fVslider17;
	FAUSTFLOAT fVslider18;
	FAUSTFLOAT fVslider19;
	FAUSTFLOAT fVslider20;
	float fRec5[2];
	FAUSTFLOAT fVbargraph2;
	FAUSTFLOAT fVslider21;
	FAUSTFLOAT fVslider22;
	float fRec6[3];
	FAUSTFLOAT fVslider23;
	FAUSTFLOAT fVslider24;
	FAUSTFLOAT fVslider25;
	FAUSTFLOAT fVslider26;
	FAUSTFLOAT fVslider27;
	float fRec7[2];
	FAUSTFLOAT fVbargraph3;
	
 public:
	
	void metadata(Meta* m) { 
		m->declare("basics.lib/name", "Faust Basic Element Library");
		m->declare("basics.lib/version", "0.3");
		m->declare("compile_options", "-a faustMinimal.h -lang cpp -es 1 -single -ftz 0");
		m->declare("filename", "multiband.dsp");
		m->declare("filters.lib/bandpass0_bandstop1:author", "Julius O. Smith III");
		m->declare("filters.lib/bandpass0_bandstop1:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/bandpass0_bandstop1:license", "MIT-style STK-4.3 license");
		m->declare("filters.lib/bandpass:author", "Julius O. Smith III");
		m->declare("filters.lib/bandpass:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/bandpass:license", "MIT-style STK-4.3 license");
		m->declare("filters.lib/fir:author", "Julius O. Smith III");
		m->declare("filters.lib/fir:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/fir:license", "MIT-style STK-4.3 license");
		m->declare("filters.lib/iir:author", "Julius O. Smith III");
		m->declare("filters.lib/iir:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/iir:license", "MIT-style STK-4.3 license");
		m->declare("filters.lib/lowpass0_highpass1", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/name", "Faust Filters Library");
		m->declare("filters.lib/tf1sb:author", "Julius O. Smith III");
		m->declare("filters.lib/tf1sb:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/tf1sb:license", "MIT-style STK-4.3 license");
		m->declare("filters.lib/tf2:author", "Julius O. Smith III");
		m->declare("filters.lib/tf2:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/tf2:license", "MIT-style STK-4.3 license");
		m->declare("filters.lib/version", "0.3");
		m->declare("maths.lib/author", "GRAME");
		m->declare("maths.lib/copyright", "GRAME");
		m->declare("maths.lib/license", "LGPL with exception");
		m->declare("maths.lib/name", "Faust Math Library");
		m->declare("maths.lib/version", "2.5");
		m->declare("name", "multiband");
		m->declare("platform.lib/name", "Generic Platform Library");
		m->declare("platform.lib/version", "0.2");
	}

	virtual int getNumInputs() {
		return 1;
	}
	virtual int getNumOutputs() {
		return 5;
	}
	
	static void classInit(int sample_rate) {
	}
	
	virtual void instanceConstants(int sample_rate) {
		fSampleRate = sample_rate;
		float fConst0 = std::min<float>(192000.0f, std::max<float>(1.0f, float(fSampleRate)));
		fConst1 = (2.0f / fConst0);
		fConst2 = (2.0f * fConst0);
		fConst3 = (3.14159274f / fConst0);
		fConst4 = (0.5f / fConst0);
		fConst5 = (4.0f * Multiband_faustpower2_f(fConst0));
		fConst6 = (1.0f / fConst0);
		fConst7 = Multiband_faustpower2_f(fConst6);
		fConst8 = (2.0f * fConst7);
	}
	
	virtual void instanceResetUserInterface() {
		fVslider0 = FAUSTFLOAT(1000.0f);
		fVslider1 = FAUSTFLOAT(6.0f);
		fVslider2 = FAUSTFLOAT(0.0f);
		fVslider3 = FAUSTFLOAT(1.0f);
		fVslider4 = FAUSTFLOAT(0.001f);
		fVslider5 = FAUSTFLOAT(0.001f);
		fVslider6 = FAUSTFLOAT(0.0f);
		fVslider7 = FAUSTFLOAT(1000.0f);
		fVslider8 = FAUSTFLOAT(6.0f);
		fVslider9 = FAUSTFLOAT(0.0f);
		fVslider10 = FAUSTFLOAT(1.0f);
		fVslider11 = FAUSTFLOAT(0.001f);
		fVslider12 = FAUSTFLOAT(0.001f);
		fVslider13 = FAUSTFLOAT(0.0f);
		fVslider14 = FAUSTFLOAT(1000.0f);
		fVslider15 = FAUSTFLOAT(6.0f);
		fVslider16 = FAUSTFLOAT(0.0f);
		fVslider17 = FAUSTFLOAT(1.0f);
		fVslider18 = FAUSTFLOAT(0.001f);
		fVslider19 = FAUSTFLOAT(0.001f);
		fVslider20 = FAUSTFLOAT(0.0f);
		fVslider21 = FAUSTFLOAT(1000.0f);
		fVslider22 = FAUSTFLOAT(6.0f);
		fVslider23 = FAUSTFLOAT(0.0f);
		fVslider24 = FAUSTFLOAT(1.0f);
		fVslider25 = FAUSTFLOAT(0.001f);
		fVslider26 = FAUSTFLOAT(0.001f);
		fVslider27 = FAUSTFLOAT(0.0f);
	}
	
	virtual void instanceClear() {
		for (int l0 = 0; (l0 < 3); l0 = (l0 + 1)) {
			fRec0[l0] = 0.0f;
		}
		for (int l1 = 0; (l1 < 2); l1 = (l1 + 1)) {
			fRec1[l1] = 0.0f;
		}
		for (int l2 = 0; (l2 < 3); l2 = (l2 + 1)) {
			fRec2[l2] = 0.0f;
		}
		for (int l3 = 0; (l3 < 2); l3 = (l3 + 1)) {
			fRec3[l3] = 0.0f;
		}
		for (int l4 = 0; (l4 < 3); l4 = (l4 + 1)) {
			fRec4[l4] = 0.0f;
		}
		for (int l5 = 0; (l5 < 2); l5 = (l5 + 1)) {
			fRec5[l5] = 0.0f;
		}
		for (int l6 = 0; (l6 < 3); l6 = (l6 + 1)) {
			fRec6[l6] = 0.0f;
		}
		for (int l7 = 0; (l7 < 2); l7 = (l7 + 1)) {
			fRec7[l7] = 0.0f;
		}
	}
	
	virtual void init(int sample_rate) {
		classInit(sample_rate);
		instanceInit(sample_rate);
	}
	virtual void instanceInit(int sample_rate) {
		instanceConstants(sample_rate);
		instanceResetUserInterface();
		instanceClear();
	}
	
	virtual Multiband* clone() {
		return new Multiband();
	}
	
	virtual int getSampleRate() {
		return fSampleRate;
	}
	
	virtual void buildUserInterface(UI* ui_interface) {
		ui_interface->openHorizontalBox("MB");
		ui_interface->openHorizontalBox("Band0");
		ui_interface->declare(&fVslider4, "scale", "log");
		ui_interface->addVerticalSlider("Attack", &fVslider4, FAUSTFLOAT(0.00100000005f), FAUSTFLOAT(9.99999975e-05f), FAUSTFLOAT(0.100000001f), FAUSTFLOAT(9.99999975e-05f));
		ui_interface->declare(&fVslider0, "scale", "log");
		ui_interface->addVerticalSlider("CenterF", &fVslider0, FAUSTFLOAT(1000.0f), FAUSTFLOAT(20.0f), FAUSTFLOAT(20000.0f), FAUSTFLOAT(0.100000001f));
		ui_interface->addVerticalBargraph("GR", &fVbargraph0, FAUSTFLOAT(-20.0f), FAUSTFLOAT(20.0f));
		ui_interface->addVerticalSlider("Gain", &fVslider2, FAUSTFLOAT(0.0f), FAUSTFLOAT(-20.0f), FAUSTFLOAT(20.0f), FAUSTFLOAT(0.100000001f));
		ui_interface->declare(&fVslider1, "scale", "log");
		ui_interface->addVerticalSlider("Quality", &fVslider1, FAUSTFLOAT(6.0f), FAUSTFLOAT(0.50999999f), FAUSTFLOAT(10.0f), FAUSTFLOAT(0.00999999978f));
		ui_interface->declare(&fVslider3, "scale", "log");
		ui_interface->addVerticalSlider("Ratio", &fVslider3, FAUSTFLOAT(1.0f), FAUSTFLOAT(0.00999999978f), FAUSTFLOAT(100.0f), FAUSTFLOAT(0.00100000005f));
		ui_interface->declare(&fVslider5, "scale", "log");
		ui_interface->addVerticalSlider("Release", &fVslider5, FAUSTFLOAT(0.00100000005f), FAUSTFLOAT(9.99999975e-05f), FAUSTFLOAT(0.100000001f), FAUSTFLOAT(9.99999975e-05f));
		ui_interface->addVerticalSlider("Threshold", &fVslider6, FAUSTFLOAT(0.0f), FAUSTFLOAT(-80.0f), FAUSTFLOAT(0.0f), FAUSTFLOAT(0.100000001f));
		ui_interface->closeBox();
		ui_interface->openHorizontalBox("Band1");
		ui_interface->declare(&fVslider11, "scale", "log");
		ui_interface->addVerticalSlider("Attack", &fVslider11, FAUSTFLOAT(0.00100000005f), FAUSTFLOAT(9.99999975e-05f), FAUSTFLOAT(0.100000001f), FAUSTFLOAT(9.99999975e-05f));
		ui_interface->declare(&fVslider7, "scale", "log");
		ui_interface->addVerticalSlider("CenterF", &fVslider7, FAUSTFLOAT(1000.0f), FAUSTFLOAT(20.0f), FAUSTFLOAT(20000.0f), FAUSTFLOAT(0.100000001f));
		ui_interface->addVerticalBargraph("GR", &fVbargraph1, FAUSTFLOAT(-20.0f), FAUSTFLOAT(20.0f));
		ui_interface->addVerticalSlider("Gain", &fVslider9, FAUSTFLOAT(0.0f), FAUSTFLOAT(-20.0f), FAUSTFLOAT(20.0f), FAUSTFLOAT(0.100000001f));
		ui_interface->declare(&fVslider8, "scale", "log");
		ui_interface->addVerticalSlider("Quality", &fVslider8, FAUSTFLOAT(6.0f), FAUSTFLOAT(0.50999999f), FAUSTFLOAT(10.0f), FAUSTFLOAT(0.00999999978f));
		ui_interface->declare(&fVslider10, "scale", "log");
		ui_interface->addVerticalSlider("Ratio", &fVslider10, FAUSTFLOAT(1.0f), FAUSTFLOAT(0.00999999978f), FAUSTFLOAT(100.0f), FAUSTFLOAT(0.00100000005f));
		ui_interface->declare(&fVslider12, "scale", "log");
		ui_interface->addVerticalSlider("Release", &fVslider12, FAUSTFLOAT(0.00100000005f), FAUSTFLOAT(9.99999975e-05f), FAUSTFLOAT(0.100000001f), FAUSTFLOAT(9.99999975e-05f));
		ui_interface->addVerticalSlider("Threshold", &fVslider13, FAUSTFLOAT(0.0f), FAUSTFLOAT(-80.0f), FAUSTFLOAT(0.0f), FAUSTFLOAT(0.100000001f));
		ui_interface->closeBox();
		ui_interface->openHorizontalBox("Band2");
		ui_interface->declare(&fVslider18, "scale", "log");
		ui_interface->addVerticalSlider("Attack", &fVslider18, FAUSTFLOAT(0.00100000005f), FAUSTFLOAT(9.99999975e-05f), FAUSTFLOAT(0.100000001f), FAUSTFLOAT(9.99999975e-05f));
		ui_interface->declare(&fVslider14, "scale", "log");
		ui_interface->addVerticalSlider("CenterF", &fVslider14, FAUSTFLOAT(1000.0f), FAUSTFLOAT(20.0f), FAUSTFLOAT(20000.0f), FAUSTFLOAT(0.100000001f));
		ui_interface->addVerticalBargraph("GR", &fVbargraph2, FAUSTFLOAT(-20.0f), FAUSTFLOAT(20.0f));
		ui_interface->addVerticalSlider("Gain", &fVslider16, FAUSTFLOAT(0.0f), FAUSTFLOAT(-20.0f), FAUSTFLOAT(20.0f), FAUSTFLOAT(0.100000001f));
		ui_interface->declare(&fVslider15, "scale", "log");
		ui_interface->addVerticalSlider("Quality", &fVslider15, FAUSTFLOAT(6.0f), FAUSTFLOAT(0.50999999f), FAUSTFLOAT(10.0f), FAUSTFLOAT(0.00999999978f));
		ui_interface->declare(&fVslider17, "scale", "log");
		ui_interface->addVerticalSlider("Ratio", &fVslider17, FAUSTFLOAT(1.0f), FAUSTFLOAT(0.00999999978f), FAUSTFLOAT(100.0f), FAUSTFLOAT(0.00100000005f));
		ui_interface->declare(&fVslider19, "scale", "log");
		ui_interface->addVerticalSlider("Release", &fVslider19, FAUSTFLOAT(0.00100000005f), FAUSTFLOAT(9.99999975e-05f), FAUSTFLOAT(0.100000001f), FAUSTFLOAT(9.99999975e-05f));
		ui_interface->addVerticalSlider("Threshold", &fVslider20, FAUSTFLOAT(0.0f), FAUSTFLOAT(-80.0f), FAUSTFLOAT(0.0f), FAUSTFLOAT(0.100000001f));
		ui_interface->closeBox();
		ui_interface->openHorizontalBox("Band3");
		ui_interface->declare(&fVslider25, "scale", "log");
		ui_interface->addVerticalSlider("Attack", &fVslider25, FAUSTFLOAT(0.00100000005f), FAUSTFLOAT(9.99999975e-05f), FAUSTFLOAT(0.100000001f), FAUSTFLOAT(9.99999975e-05f));
		ui_interface->declare(&fVslider21, "scale", "log");
		ui_interface->addVerticalSlider("CenterF", &fVslider21, FAUSTFLOAT(1000.0f), FAUSTFLOAT(20.0f), FAUSTFLOAT(20000.0f), FAUSTFLOAT(0.100000001f));
		ui_interface->addVerticalBargraph("GR", &fVbargraph3, FAUSTFLOAT(-20.0f), FAUSTFLOAT(20.0f));
		ui_interface->addVerticalSlider("Gain", &fVslider23, FAUSTFLOAT(0.0f), FAUSTFLOAT(-20.0f), FAUSTFLOAT(20.0f), FAUSTFLOAT(0.100000001f));
		ui_interface->declare(&fVslider22, "scale", "log");
		ui_interface->addVerticalSlider("Quality", &fVslider22, FAUSTFLOAT(6.0f), FAUSTFLOAT(0.50999999f), FAUSTFLOAT(10.0f), FAUSTFLOAT(0.00999999978f));
		ui_interface->declare(&fVslider24, "scale", "log");
		ui_interface->addVerticalSlider("Ratio", &fVslider24, FAUSTFLOAT(1.0f), FAUSTFLOAT(0.00999999978f), FAUSTFLOAT(100.0f), FAUSTFLOAT(0.00100000005f));
		ui_interface->declare(&fVslider26, "scale", "log");
		ui_interface->addVerticalSlider("Release", &fVslider26, FAUSTFLOAT(0.00100000005f), FAUSTFLOAT(9.99999975e-05f), FAUSTFLOAT(0.100000001f), FAUSTFLOAT(9.99999975e-05f));
		ui_interface->addVerticalSlider("Threshold", &fVslider27, FAUSTFLOAT(0.0f), FAUSTFLOAT(-80.0f), FAUSTFLOAT(0.0f), FAUSTFLOAT(0.100000001f));
		ui_interface->closeBox();
		ui_interface->closeBox();
	}
	
	virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) {
		FAUSTFLOAT* input0 = inputs[0];
		FAUSTFLOAT* output0 = outputs[0];
		FAUSTFLOAT* output1 = outputs[1];
		FAUSTFLOAT* output2 = outputs[2];
		FAUSTFLOAT* output3 = outputs[3];
		FAUSTFLOAT* output4 = outputs[4];
		float fSlow0 = float(fVslider0);
		float fSlow1 = (0.5f / float(fVslider1));
		float fSlow2 = std::tan((fConst3 * std::min<float>((fSlow0 * (fSlow1 + 1.0f)), 20000.0f)));
		float fSlow3 = Multiband_faustpower2_f(std::sqrt((fConst5 * (std::tan((fConst3 * std::max<float>((fSlow0 * (1.0f - fSlow1)), 20.0f))) * fSlow2))));
		float fSlow4 = ((fConst2 * fSlow2) - (fConst4 * (fSlow3 / fSlow2)));
		float fSlow5 = (fConst7 * fSlow3);
		float fSlow6 = (fConst1 * fSlow4);
		float fSlow7 = ((fSlow5 + fSlow6) + 4.0f);
		float fSlow8 = (fConst1 * (fSlow4 / fSlow7));
		float fSlow9 = (1.0f / fSlow7);
		float fSlow10 = ((fConst8 * fSlow3) + -8.0f);
		float fSlow11 = (fSlow5 + (4.0f - fSlow6));
		float fSlow12 = (0.0f - fSlow8);
		float fSlow13 = float(fVslider2);
		float fSlow14 = (1.0f / float(fVslider3));
		float fSlow15 = (0.100000001f * (0.0f - (1.0f - fSlow14)));
		float fSlow16 = (1.0f - std::exp((0.0f - (fConst6 / float(fVslider4)))));
		float fSlow17 = (1.0f - std::exp((0.0f - (fConst6 / float(fVslider5)))));
		float fSlow18 = float(fVslider6);
		float fSlow19 = (fSlow18 + -2.5f);
		float fSlow20 = std::pow(10.0f, (0.0500000007f * fSlow19));
		float fSlow21 = (fSlow18 + 2.5f);
		float fSlow22 = (fSlow14 + -1.0f);
		float fSlow23 = float(fVslider7);
		float fSlow24 = (0.5f / float(fVslider8));
		float fSlow25 = std::tan((fConst3 * std::min<float>((fSlow23 * (fSlow24 + 1.0f)), 20000.0f)));
		float fSlow26 = Multiband_faustpower2_f(std::sqrt((fConst5 * (std::tan((fConst3 * std::max<float>((fSlow23 * (1.0f - fSlow24)), 20.0f))) * fSlow25))));
		float fSlow27 = ((fConst2 * fSlow25) - (fConst4 * (fSlow26 / fSlow25)));
		float fSlow28 = (fConst7 * fSlow26);
		float fSlow29 = (fConst1 * fSlow27);
		float fSlow30 = ((fSlow28 + fSlow29) + 4.0f);
		float fSlow31 = (fConst1 * (fSlow27 / fSlow30));
		float fSlow32 = (1.0f / fSlow30);
		float fSlow33 = ((fConst8 * fSlow26) + -8.0f);
		float fSlow34 = (fSlow28 + (4.0f - fSlow29));
		float fSlow35 = (0.0f - fSlow31);
		float fSlow36 = float(fVslider9);
		float fSlow37 = (1.0f / float(fVslider10));
		float fSlow38 = (0.100000001f * (0.0f - (1.0f - fSlow37)));
		float fSlow39 = (1.0f - std::exp((0.0f - (fConst6 / float(fVslider11)))));
		float fSlow40 = (1.0f - std::exp((0.0f - (fConst6 / float(fVslider12)))));
		float fSlow41 = float(fVslider13);
		float fSlow42 = (fSlow41 + -2.5f);
		float fSlow43 = std::pow(10.0f, (0.0500000007f * fSlow42));
		float fSlow44 = (fSlow41 + 2.5f);
		float fSlow45 = (fSlow37 + -1.0f);
		float fSlow46 = float(fVslider14);
		float fSlow47 = (0.5f / float(fVslider15));
		float fSlow48 = std::tan((fConst3 * std::min<float>((fSlow46 * (fSlow47 + 1.0f)), 20000.0f)));
		float fSlow49 = Multiband_faustpower2_f(std::sqrt((fConst5 * (std::tan((fConst3 * std::max<float>((fSlow46 * (1.0f - fSlow47)), 20.0f))) * fSlow48))));
		float fSlow50 = ((fConst2 * fSlow48) - (fConst4 * (fSlow49 / fSlow48)));
		float fSlow51 = (fConst7 * fSlow49);
		float fSlow52 = (fConst1 * fSlow50);
		float fSlow53 = ((fSlow51 + fSlow52) + 4.0f);
		float fSlow54 = (fConst1 * (fSlow50 / fSlow53));
		float fSlow55 = (1.0f / fSlow53);
		float fSlow56 = ((fConst8 * fSlow49) + -8.0f);
		float fSlow57 = (fSlow51 + (4.0f - fSlow52));
		float fSlow58 = (0.0f - fSlow54);
		float fSlow59 = float(fVslider16);
		float fSlow60 = (1.0f / float(fVslider17));
		float fSlow61 = (0.100000001f * (0.0f - (1.0f - fSlow60)));
		float fSlow62 = (1.0f - std::exp((0.0f - (fConst6 / float(fVslider18)))));
		float fSlow63 = (1.0f - std::exp((0.0f - (fConst6 / float(fVslider19)))));
		float fSlow64 = float(fVslider20);
		float fSlow65 = (fSlow64 + -2.5f);
		float fSlow66 = std::pow(10.0f, (0.0500000007f * fSlow65));
		float fSlow67 = (fSlow64 + 2.5f);
		float fSlow68 = (fSlow60 + -1.0f);
		float fSlow69 = float(fVslider21);
		float fSlow70 = (0.5f / float(fVslider22));
		float fSlow71 = std::tan((fConst3 * std::min<float>((fSlow69 * (fSlow70 + 1.0f)), 20000.0f)));
		float fSlow72 = Multiband_faustpower2_f(std::sqrt((fConst5 * (std::tan((fConst3 * std::max<float>((fSlow69 * (1.0f - fSlow70)), 20.0f))) * fSlow71))));
		float fSlow73 = ((fConst2 * fSlow71) - (fConst4 * (fSlow72 / fSlow71)));
		float fSlow74 = (fConst7 * fSlow72);
		float fSlow75 = (fConst1 * fSlow73);
		float fSlow76 = ((fSlow74 + fSlow75) + 4.0f);
		float fSlow77 = (fConst1 * (fSlow73 / fSlow76));
		float fSlow78 = (1.0f / fSlow76);
		float fSlow79 = ((fConst8 * fSlow72) + -8.0f);
		float fSlow80 = (fSlow74 + (4.0f - fSlow75));
		float fSlow81 = (0.0f - fSlow77);
		float fSlow82 = float(fVslider23);
		float fSlow83 = (1.0f / float(fVslider24));
		float fSlow84 = (0.100000001f * (0.0f - (1.0f - fSlow83)));
		float fSlow85 = (1.0f - std::exp((0.0f - (fConst6 / float(fVslider25)))));
		float fSlow86 = (1.0f - std::exp((0.0f - (fConst6 / float(fVslider26)))));
		float fSlow87 = float(fVslider27);
		float fSlow88 = (fSlow87 + -2.5f);
		float fSlow89 = std::pow(10.0f, (0.0500000007f * fSlow88));
		float fSlow90 = (fSlow87 + 2.5f);
		float fSlow91 = (fSlow83 + -1.0f);
		for (int i0 = 0; (i0 < count); i0 = (i0 + 1)) {
			float fTemp0 = float(input0[i0]);
			fRec0[0] = (fTemp0 - (fSlow9 * ((fSlow10 * fRec0[1]) + (fSlow11 * fRec0[2]))));
			float fTemp1 = ((fSlow8 * fRec0[0]) + (fSlow12 * fRec0[2]));
			float fTemp2 = std::fabs(fTemp1);
			fRec1[0] = ((fRec1[1] + (fSlow16 * ((fTemp2 - fRec1[1]) * float((fTemp2 >= fRec1[1]))))) + (fSlow17 * ((fSlow20 - fRec1[1]) * float((fTemp2 < fRec1[1])))));
			float fTemp3 = (20.0f * std::log10(std::max<float>(1.17549435e-38f, fRec1[0])));
			fVbargraph0 = FAUSTFLOAT((fSlow13 + ((fSlow15 * ((float((fTemp3 < fSlow21)) * Multiband_faustpower2_f(((fTemp3 + 2.5f) - fSlow18))) * float((fTemp3 >= fSlow19)))) + (fSlow22 * ((fTemp3 - fSlow18) * float((fTemp3 > fSlow21)))))));
			float fTemp4 = std::pow(10.0f, (0.0500000007f * fVbargraph0));
			fRec2[0] = (fTemp0 - (fSlow32 * ((fSlow33 * fRec2[1]) + (fSlow34 * fRec2[2]))));
			float fTemp5 = ((fSlow31 * fRec2[0]) + (fSlow35 * fRec2[2]));
			float fTemp6 = std::fabs(fTemp5);
			fRec3[0] = ((fRec3[1] + (fSlow39 * ((fTemp6 - fRec3[1]) * float((fTemp6 >= fRec3[1]))))) + (fSlow40 * ((fSlow43 - fRec3[1]) * float((fTemp6 < fRec3[1])))));
			float fTemp7 = (20.0f * std::log10(std::max<float>(1.17549435e-38f, fRec3[0])));
			fVbargraph1 = FAUSTFLOAT((fSlow36 + ((fSlow38 * ((float((fTemp7 < fSlow44)) * Multiband_faustpower2_f(((fTemp7 + 2.5f) - fSlow41))) * float((fTemp7 >= fSlow42)))) + (fSlow45 * ((fTemp7 - fSlow41) * float((fTemp7 > fSlow44)))))));
			float fTemp8 = std::pow(10.0f, (0.0500000007f * fVbargraph1));
			fRec4[0] = (fTemp0 - (fSlow55 * ((fSlow56 * fRec4[1]) + (fSlow57 * fRec4[2]))));
			float fTemp9 = ((fSlow54 * fRec4[0]) + (fSlow58 * fRec4[2]));
			float fTemp10 = std::fabs(fTemp9);
			fRec5[0] = ((fRec5[1] + (fSlow62 * ((fTemp10 - fRec5[1]) * float((fTemp10 >= fRec5[1]))))) + (fSlow63 * ((fSlow66 - fRec5[1]) * float((fTemp10 < fRec5[1])))));
			float fTemp11 = (20.0f * std::log10(std::max<float>(1.17549435e-38f, fRec5[0])));
			fVbargraph2 = FAUSTFLOAT((fSlow59 + ((fSlow61 * ((float((fTemp11 < fSlow67)) * Multiband_faustpower2_f(((fTemp11 + 2.5f) - fSlow64))) * float((fTemp11 >= fSlow65)))) + (fSlow68 * ((fTemp11 - fSlow64) * float((fTemp11 > fSlow67)))))));
			float fTemp12 = std::pow(10.0f, (0.0500000007f * fVbargraph2));
			fRec6[0] = (fTemp0 - (fSlow78 * ((fSlow79 * fRec6[1]) + (fSlow80 * fRec6[2]))));
			float fTemp13 = ((fSlow77 * fRec6[0]) + (fSlow81 * fRec6[2]));
			float fTemp14 = std::fabs(fTemp13);
			fRec7[0] = ((fRec7[1] + (fSlow85 * ((fTemp14 - fRec7[1]) * float((fTemp14 >= fRec7[1]))))) + (fSlow86 * ((fSlow89 - fRec7[1]) * float((fTemp14 < fRec7[1])))));
			float fTemp15 = (20.0f * std::log10(std::max<float>(1.17549435e-38f, fRec7[0])));
			fVbargraph3 = FAUSTFLOAT((fSlow82 + ((fSlow84 * ((float((fTemp15 < fSlow90)) * Multiband_faustpower2_f(((fTemp15 + 2.5f) - fSlow87))) * float((fTemp15 >= fSlow88)))) + (fSlow91 * ((fTemp15 - fSlow87) * float((fTemp15 > fSlow90)))))));
			float fTemp16 = std::pow(10.0f, (0.0500000007f * fVbargraph3));
			output0[i0] = FAUSTFLOAT((fTemp0 + ((((fTemp1 * (fTemp4 + -1.0f)) + (fTemp5 * (fTemp8 + -1.0f))) + (fTemp9 * (fTemp12 + -1.0f))) + (fTemp13 * (fTemp16 + -1.0f)))));
			output1[i0] = FAUSTFLOAT(fTemp4);
			output2[i0] = FAUSTFLOAT(fTemp8);
			output3[i0] = FAUSTFLOAT(fTemp12);
			output4[i0] = FAUSTFLOAT(fTemp16);
			fRec0[2] = fRec0[1];
			fRec0[1] = fRec0[0];
			fRec1[1] = fRec1[0];
			fRec2[2] = fRec2[1];
			fRec2[1] = fRec2[0];
			fRec3[1] = fRec3[0];
			fRec4[2] = fRec4[1];
			fRec4[1] = fRec4[0];
			fRec5[1] = fRec5[0];
			fRec6[2] = fRec6[1];
			fRec6[1] = fRec6[0];
			fRec7[1] = fRec7[0];
		}
	}

};

// END-FAUSTDSP

#endif
