/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"

//==============================================================================
// Create some static parameter IDs here
static juce::String attack{"Attack"};
static juce::String ratio{"Ratio"};
static juce::String release{"Release"};
static juce::String threshold{"Threshold"};
static juce::String centerFreq{"CenterF"};
static juce::String gain{"Gain"};
static juce::String quality{"Quality"};
static juce::String gainReduction{"GR"};

// format of parameter addresses example
// /MB/Band2/CenterF

// Visualizer static params
static juce::Identifier fftID{"fftPlot"};
static juce::String smoothTime{"smoothTime"};
static juce::String minDb{"MinDb"};
static juce::String maxDb{"MaxDb"};
// static juce::String centroid{"Centroid"};

static juce::Identifier eqID{"eqPlot"};

//  if needed, add
// #include <utility>
// to modules/juce_core/system/juce_standardHeader.h

// FOR FOLEYS_CONVERSIONS.H
// // optimised for frequencies: >3 kHz: 2 decimals
// if (value > FloatType(3000))
//   return juce::jlimit(start, end, FloatType(100) * juce::roundToInt(value / FloatType(100)));

// // optimised for frequencies: 1-3 kHz: 1 decimal
// if (value > FloatType(1000))
//   return juce::jlimit(start, end, FloatType(10) * juce::roundToInt(value / FloatType(10)));
// // optimised for frequencies: <3 :
// if (value < FloatType(3))
//   return juce::jlimit(start, end, juce::roundToInt(value *FloatType(100)) / FloatType(100));
// // optimised for frequencies: 3-10 : 2 decimals
// if (value < FloatType(10))
//   return juce::jlimit(start, end, juce::roundToInt(value *FloatType(10)) / FloatType(10));

// return juce::jlimit(start, end, FloatType(juce::roundToInt(value)));
//==============================================================================

juce::AudioProcessorValueTreeState::ParameterLayout FaustAndJuceAudioProcessor::createParameterLayout()
{
  juce::AudioProcessorValueTreeState::ParameterLayout layout;
  // PGM <-> faust params
  for (int i = 0; i < 4; i++)
  {
    layout.add(std::make_unique<juce::AudioParameterFloat>(
         attack + juce::String(i),
        attack,
        juce::NormalisableRange<float>(0.0001, 0.1, 0.0001),
        0.02,
        juce::String(),                                  // parameterLabel
        juce::AudioProcessorParameter::genericParameter, // parameter category
        [](float value, int)
        { return (value>0.001f) ? juce::String((int)(value * 1000)) + juce::String(" ms") : juce::String((int)(value * 1000000))  + juce::String(" us"); }, // stringFromValue lambda
        [](juce::String text)
        { return text.getFloatValue() / 1000; }));
    layout.add(std::make_unique<juce::AudioParameterFloat>(
        release + juce::String(i),
        release,
        juce::NormalisableRange<float>(0.01, 0.5, 0.01),
        0.2,
        juce::String(),                                  // parameterLabel
        juce::AudioProcessorParameter::genericParameter, // parameter category
        [](float value, int)
        { return juce::String((int)(value * 1000)) + juce::String(" ms"); }, // stringFromValue lambda
        [](juce::String text)
        { return text.getFloatValue() / 1000; }));
    layout.add(std::make_unique<juce::AudioParameterFloat>(
        ratio + juce::String(i),                                // parameter name
        ratio,                                                         // parameterID
        foleys::Conversions::makeLogarithmicRangeEdited<float>(.1f, 100.0f), // range
        4.0f,                                                          // initial/defualt value
        juce::String(),                                                // parameterLabel
        juce::AudioProcessorParameter::genericParameter,               // parameter category
        [](float value, int)
        { return juce::String(value); }, // stringFromValue lambda
        [](juce::String text)
        { return text.getFloatValue(); })); // valueFromString lambda
    layout.add(std::make_unique<juce::AudioParameterFloat>((juce::String() += threshold) += i, threshold, juce::NormalisableRange<float>(-80, 0, .1), 0));
    layout.add(std::make_unique<juce::AudioParameterFloat>(
        centerFreq + juce::String(i),
        centerFreq,
        foleys::Conversions::makeLogarithmicRangeEdited<float>(20.0f, 20000.0f),
        (100.0f * pow(4, i)),
        juce::String(),
        juce::AudioProcessorParameter::genericParameter,
        [](float value, int)
        { return (value < 1000) ? juce::String(value, 0) + " Hz" : juce::String(value / 1000.0) + " kHz"; },
        [](juce::String text)
        { return text.endsWith(" kHz") ? text.getFloatValue() * 1000.0f : text.getFloatValue(); }));
    layout.add(std::make_unique<juce::AudioParameterFloat>(
        quality + juce::String(i),
        quality,
        foleys::Conversions::makeLogarithmicRangeEdited<float>(.55f, 20.0f), // range
        4.0f,                                                          // initial/defualt value
        juce::String(),                                                // parameterLabel
        juce::AudioProcessorParameter::genericParameter,               // parameter category
        [](float value, int)
        { return juce::String(value); }, // stringFromValue lambda
        [](juce::String text)
        { return text.getFloatValue(); })); // valueFromString lambda
    layout.add(std::make_unique<juce::AudioParameterFloat>(
        gain + juce::String(i),
        gain,
        juce::NormalisableRange<float>(-30, 30, .1), 0,
        juce::String(),                                  // parameterLabel
        juce::AudioProcessorParameter::genericParameter, // parameter category
        [](float value, int)
        { return juce::String(value) + " dB"; }, // stringFromValue lambda
        [](juce::String text)
        { return text.getFloatValue(); })); // valueFromString lambda);
    layout.add(std::make_unique<juce::AudioParameterFloat>(
        gainReduction + juce::String(i),
        gainReduction,
        juce::NormalisableRange<float>(-60, 60, .1),
        0,
        juce::String(),                                  // parameterLabel
        juce::AudioProcessorParameter::genericParameter, // parameter category
        [](float value, int)
        { return juce::String(value) + " dB"; }, // stringFromValue lambda
        [](juce::String text)
        { return text.getFloatValue(); }));
  }
  // visualizer params
  layout.add(std::make_unique<juce::AudioParameterFloat>(smoothTime, "Smooth Time (ms)", juce::NormalisableRange<float>(0, 500, 1), 100));
  layout.add(std::make_unique<juce::AudioParameterFloat>(minDb, "Minimum Db", juce::NormalisableRange<float>(-120, 0, 1), -30));
  layout.add(std::make_unique<juce::AudioParameterFloat>(maxDb, "Maximum Db", juce::NormalisableRange<float>(0, 50, 1), 30));
  // layout.add(std::make_unique<juce::AudioParameterFloat>(centroid, "Spectral Centroid", juce::NormalisableRange<float>(20, 20000, .1), 1000));

  return layout;
}

//==============================================================================
FaustAndJuceAudioProcessor::FaustAndJuceAudioProcessor()
    : // One or two initializations:
      // initialize the FFT parts
      forwardFFT(FFT_ORDER), window(FFT_SIZE, juce::dsp::WindowingFunction<float>::hann),
#ifndef JucePlugin_PreferredChannelConfigurations
      foleys::MagicProcessor(BusesProperties()
#if !JucePlugin_IsMidiEffect
#if !JucePlugin_IsSynth
                                 .withInput("Input", juce::AudioChannelSet::mono(), true)
#endif
                                 .withOutput("Output", juce::AudioChannelSet::mono(), true)
#endif
                                 ),
#endif
      treeState(*this, nullptr, JucePlugin_Name, createParameterLayout())
{
  // FAUST:
  fMB = std::make_unique<Faust::Multiband>();
  fUI = std::make_unique<Faust::MapUI>();

  // Attach UI to our DSP
  fMB->buildUserInterface(fUI.get());
  // PGM: ADD PLOTS AND LISTENERS

  // add a meter for the output
  outputMeter = magicState.createAndAddObject<foleys::MagicLevelSource>("outputMeter");

  // add meters for the gain reduction
  for (int i = 0; i < 4; ++i) {
    grMeters.push_back(magicState.createAndAddObject<foleys::MagicLevelSource>(juce::String("grMeter") + juce::String(i)));
  }

  // Hooking up our EQ plot
  eqPlot = magicState.createAndAddObject<Visualizer>(eqID, &eqData[0], FFT_SIZE / 2);

  // Hooking up our FFT plot
  // Constructor needs pointer to data and data size
  fftPlot = magicState.createAndAddObject<Visualizer>(fftID, &fftData[0], FFT_SIZE / 2);
  
  // add a listener for each paramater value so that it actually changes
  treeState.addParameterListener(smoothTime, this);
  treeState.addParameterListener(minDb, this);
  treeState.addParameterListener(maxDb, this);
  // control params

  for (int i = 0; i < 4; i++)
  {
    treeState.addParameterListener(attack + juce::String(i), this);
    treeState.addParameterListener(release + juce::String(i), this);
    treeState.addParameterListener(ratio + juce::String(i), this);
    treeState.addParameterListener(threshold + juce::String(i), this);
    treeState.addParameterListener(centerFreq + juce::String(i), this);
    treeState.addParameterListener(quality + juce::String(i), this);
    treeState.addParameterListener(gain + juce::String(i), this);
  }
  // add a listener for each paramater value so that it actually changes

  // load PGM layout from xml
  magicState.setGuiValueTree(BinaryData::layout_xml, BinaryData::layout_xmlSize);

}

FaustAndJuceAudioProcessor::~FaustAndJuceAudioProcessor()
{
}

//==============================================================================
const juce::String FaustAndJuceAudioProcessor::getName() const
{
  return JucePlugin_Name;
}

bool FaustAndJuceAudioProcessor::acceptsMidi() const
{
#if JucePlugin_WantsMidiInput
  return true;
#else
  return false;
#endif
}

bool FaustAndJuceAudioProcessor::producesMidi() const
{
#if JucePlugin_ProducesMidiOutput
  return true;
#else
  return false;
#endif
}

bool FaustAndJuceAudioProcessor::isMidiEffect() const
{
#if JucePlugin_IsMidiEffect
  return true;
#else
  return false;
#endif
}

double FaustAndJuceAudioProcessor::getTailLengthSeconds() const
{
  return 0.0;
}

int FaustAndJuceAudioProcessor::getNumPrograms()
{
  return 1; // NB: some hosts don't cope very well if you tell them there are 0 programs,
            // so this should be at least 1, even if you're not really implementing programs.
}

int FaustAndJuceAudioProcessor::getCurrentProgram()
{
  return 0;
}

void FaustAndJuceAudioProcessor::setCurrentProgram(int index)
{
}

const juce::String FaustAndJuceAudioProcessor::getProgramName(int index)
{
  return {};
}

void FaustAndJuceAudioProcessor::changeProgramName(int index, const juce::String &newName)
{
}

//==============================================================================
void FaustAndJuceAudioProcessor::prepareToPlay(double sampleRate, int samplesPerBlock)
{

  // FAUST initialization
  fMB->init(sampleRate);
  
  // FFT PLOT initialization
  // zero some arrays
  juce::zeromem(newFftData, sizeof(newFftData));
  juce::zeromem(oldFftData, sizeof(oldFftData));

  // sample rate for leak calculations
  fs = sampleRate;
  // initialize leak of FFT smoothing value to 150ms
  leak = std::exp(-(FFT_SIZE) / (150.0 * 0.001 * fs));

  // initialize frequency lines
  for (int i = 0; i < FFT_SIZE / 2; ++i)
  {
    frequencies[i] = i * fs / FFT_SIZE;
  }
  // Hook up frequencies to our x-axis data
  fftPlot->setX(&frequencies[0]);

  // hook the plot minimums and maximums to the minimums and maximums
  fftPlot->setDbBounds(minDbVal, maxDbVal);

  // EQ PLOT initialization
  // setup frequencies for the EQ plot
  float multiplier = pow(1000.0f, 1.0f / FFT_SIZE * 2);
  eqFreqs[0] = 20;
  for (int i = 1; i < FFT_SIZE / 2; ++i)
    eqFreqs[i] = eqFreqs[i - 1] * multiplier;

  // initialize the EQ curve plot & initial update
  eqPlot->setX(&eqFreqs[0]);
  eqPlot->setDbBounds(minDbVal, maxDbVal);

  // Give our magicState the required sampling rate and samples per block
  magicState.prepareToPlay(sampleRate, samplesPerBlock);
  // Give the output for our meter
  outputMeter->setupSource(getTotalNumOutputChannels(), sampleRate, 500);

  for(int i =0; i < 4; i++){
    grMeters[i]->setupSource(1, sampleRate, 500);
  }

  // manually set all of the faust parameter values
  for(int band = 0; band < 4; band++){
    juce::String bandString = juce::String("/MB/Band") + juce::String(band) + juce::String("/") ;
    // attack, release, threshold, ratio, gain, quality, centerFrequency
    fUI->setParamValue((bandString+attack).toStdString(), *treeState.getRawParameterValue(attack + juce::String(band)));
    fUI->setParamValue((bandString+release).toStdString(), *treeState.getRawParameterValue(release + juce::String(band)));
    fUI->setParamValue((bandString+threshold).toStdString(), *treeState.getRawParameterValue(threshold + juce::String(band)));
    fUI->setParamValue((bandString+ratio).toStdString(), *treeState.getRawParameterValue(ratio + juce::String(band)));
    fUI->setParamValue((bandString+gain).toStdString(), *treeState.getRawParameterValue(gain + juce::String(band)));
    fUI->setParamValue((bandString+quality).toStdString(), *treeState.getRawParameterValue(quality + juce::String(band)));
    fUI->setParamValue((bandString+centerFreq).toStdString(), *treeState.getRawParameterValue(centerFreq + juce::String(band)));
  }

  // set the initial eq band values
  for (int i = 0; i < 4; i++)
  {
    updateBand(i);
  }
  // set the correct eq plot
  updateEqPlot();

}

void FaustAndJuceAudioProcessor::releaseResources()
{
  // When playback stops, you can use this as an opportunity to free up any
  // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool FaustAndJuceAudioProcessor::isBusesLayoutSupported(const BusesLayout &layouts) const
{
#if JucePlugin_IsMidiEffect
  juce::ignoreUnused(layouts);
  return true;
#else
  // This is the place where you check if the layout is supported.
  // In this template code we only support mono or stereo.
  // Some plugin hosts, such as certain GarageBand versions, will only
  // load plugins that support stereo bus layouts.
  if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono() && layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
    return false;

    // This checks if the input layout matches the output layout
#if !JucePlugin_IsSynth
  if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
    return false;
#endif

  return true;
#endif
}
#endif

void FaustAndJuceAudioProcessor::processBlock(juce::AudioBuffer<float> &buffer, juce::MidiBuffer &midiMessages)
{
  juce::ScopedNoDenormals noDenormals;
  auto totalNumInputChannels = getTotalNumInputChannels();
  auto totalNumOutputChannels = getTotalNumOutputChannels();

  // do audio processing in Faust
  int numSamples = buffer.getNumSamples();

  // gr status is passed as output to faust
  juce::AudioBuffer<float> grBuffer;
  grBuffer.setSize(5, numSamples);

  faustIn = buffer.getArrayOfWritePointers();
  float ** faustOutPtr = grBuffer.getArrayOfWritePointers();

  fMB->compute(numSamples, faustIn, faustOutPtr);

  // copy faust output to the relevant locations
  if (totalNumInputChannels > 0){
    // copy the audio part of the faust output to the buffer
    auto *audioOut = grBuffer.getReadPointer(0);
    buffer.copyFrom(0, 0, audioOut, numSamples);
    // send the band data to each gain reduction meter
    for(int i = 0; i < 4; i++){
      // a bit messy, lots of copying, maybe there is a way to optimize this better. 
      juce::AudioBuffer<float> grBand; 
      grBand.setSize(1, numSamples);
      grBand.copyFrom(0, 0, grBuffer.getReadPointer(1+i), numSamples);
      grMeters[i]->pushSamples(grBand);
    }
  }


  // compute the FFT for the block 
  if (totalNumInputChannels > 0)
  {
    // read data into buffer
    auto *channelData = buffer.getReadPointer(0);
    // copy samples into fft buffer
    for (int samp = 0; samp < buffer.getNumSamples(); samp++)
    {
      // copy a sample into the buffer
      fifo[fifoIndex] = channelData[samp];
      fifoIndex++;
      // if buffer is full, compute the FFT
      if (fifoIndex == FFT_SIZE)
      {
        // zero FFT temp buffer
        juce::zeromem(newFftData, sizeof(newFftData));
        // copy larger audio buffer into the fft data
        memcpy(newFftData, fifo, sizeof(fifo));
        // reset larger audio buffer index
        fifoIndex = 0;
        // preform FFT on larger audio buffer
        forwardFFT.performFrequencyOnlyForwardTransform(newFftData);
        // copy in new FFT data
        for (int i = 0; i < FFT_SIZE / 2; i++)
        {
          oldFftData[i] = leak * oldFftData[i] + (1.0 - leak) * newFftData[i];
          fftData[i] = oldFftData[i];
        }
        fftPlot->update();
      }
    }
  }

  updateEqPlot();
  outputMeter->pushSamples(buffer);
}

void FaustAndJuceAudioProcessor::updateEqPlot()
{

  // fill with ones
  std::fill_n(eqData, FFT_SIZE / 2, 1.0f);

  for (int i = 0; i < 4; i++)
  {
    juce::Value grParameter = treeState.getParameterAsValue(gainReduction  + juce::String(i));
    juce::String faustParam = juce::String("/MB/Band") + juce::String(i) + juce::String("/GR");

    // update the gr parameter for PGM to display (convenient and effecient spot to do it)
    float gr = fUI->getParamValue(faustParam.toStdString());
    
    // get rms value from GR meter
    // TODO: meters and graph that actually reflect the exact behavior of the compressor. 
    // float gr = grMeters[i]->getRMSvalue(0);

    //convert to linear scale
    // ((float(0) < gr) - (gr < float(0)))  easy signum (gr)
    float grLin =(pow(10.0f, (gr * .05f)))-1.0;
    // iterate over frequencies and add in the eq plot
    for (int j = 0; j < FFT_SIZE / 2; j++)
    {
      eqData[j] = eqData[j] + grLin * bandNeq[i][j];
    }
  }

  eqPlot->update();
}

void FaustAndJuceAudioProcessor::updateBand(int band)
{
  float fc, q, fL, fH;
  // get the needed parameter values used by faust - this doesn't work if faust is being initialized...
  // juce::String faustParam = juce::String("/MB/Band") + juce::String(band) + juce::String("/CenterF");
  // fc = fUI->getParamValue(faustParam.toStdString());
  // faustParam = juce::String("/MB/Band") + juce::String(band) + juce::String("/Quality");
  // q = fUI->getParamValue(faustParam.toStdString());

  // get the current parameter values according to JUCE
  q = *treeState.getRawParameterValue(quality + juce::String(band));
  fc = *treeState.getRawParameterValue(centerFreq + juce::String(band));

  // get the juce IIR coefficients so we can get freqs
  juce::dsp::IIR::Coefficients<float>::Ptr bandpass = juce::dsp::IIR::Coefficients<float>::makeBandPass(fs>10.0?fs:48000.0, fc, q);
  double tempFreqs[FFT_SIZE / 2];

  for (int i = 0; i < FFT_SIZE / 2; i++)
  {
    tempFreqs[i] = eqFreqs[i];
  }
  double tempAmps[FFT_SIZE / 2];

  bandpass->getMagnitudeForFrequencyArray(tempFreqs, tempAmps, FFT_SIZE / 2, fs>10.0?fs:48000.0);
  // // do the same min/max parsing as faust
  // fH = std::min(fc * (1 + 1 / (2 * q)), 20000.0f) / fs;
  // fL = std::max(fc * (1 - 1 / (2 * q)), 20.0f) / fs;

  for (int i = 0; i < FFT_SIZE / 2; i++)
  {
    bandNeq[band][i] = tempAmps[i];
  }

  // // write an array of amplitudes
  // for (int i = 1; i < FFT_SIZE / 2; ++i)
  // {
  //   float f = eqFreqs[i] / fs;
  //   float gain = 2 * 3.1415 * f / sqrt((1 + f * f / fL / fL) * (1 + f * f / fH / fH));
  //   bandNeq[band][i] = 2 * 3.1415 * f / sqrt((1 + f * f / fL / fL) * (1 + f * f / fH / fH));
  // }
}

//==============================================================================
// bool FaustAndJuceAudioProcessor::hasEditor() const
//{
//    return true; // (change this to false if you choose to not supply an editor)
//}
//
// juce::AudioProcessorEditor* FaustAndJuceAudioProcessor::createEditor()
//{
//    return new FaustAndJuceAudioProcessorEditor (*this);
//}

//==============================================================================
#if 0 // Let FaustAndJuce handle this:

void FaustAndJuceAudioProcessor::getStateInformation (juce::MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
}

void FaustAndJuceAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
}

#endif

void FaustAndJuceAudioProcessor::parameterChanged(const juce::String &parameterID, float newValue)
{
  // regular PGM controls
  if (parameterID == smoothTime)
  {
    leak = newValue < __FLT_EPSILON__ ? 0.0 : std::exp(-(FFT_SIZE) / (newValue * 0.001 * fs));
  }
  else if (parameterID == minDb)
  {
    minDbVal = newValue;
    eqPlot->setDbBounds(minDbVal, maxDbVal);
    fftPlot->setDbBounds(minDbVal, maxDbVal);
  }
  else if (parameterID == maxDb)
  {
    maxDbVal = newValue;
    eqPlot->setDbBounds(minDbVal, maxDbVal);
    fftPlot->setDbBounds(minDbVal, maxDbVal);
  }
  else // Update faust parameters using faust parameter ID
  {
    juce::String band = parameterID.getLastCharacters(1);
    juce::String parameter = parameterID.substring(0, parameterID.length() - 1);

    juce::String faustParam = juce::String("/MB/Band") + juce::String(band) + juce::String("/") + juce::String(parameter);

    fUI->setParamValue(faustParam.toStdString(), newValue);

    updateBand(((int)band.getFloatValue()));
  }
}

//==============================================================================
// This creates new instances of the plugin..
juce::AudioProcessor *JUCE_CALLTYPE createPluginFilter()
{
  return new FaustAndJuceAudioProcessor();
}
