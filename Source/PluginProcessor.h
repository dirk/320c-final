/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

// Import our Visualizer code
#include "Visualizer.h"

namespace foleys
{

    namespace Conversions
    {

        template <typename FloatType>
        static inline juce::NormalisableRange<FloatType> makeLogarithmicRangeEdited(FloatType min, FloatType max)
        {
            return juce::NormalisableRange<FloatType>(
                min, max,
                [](FloatType start, FloatType end, FloatType normalised)
                {
                    return start + (std::pow(FloatType(2), normalised * FloatType(10)) - FloatType(1)) * (end - start) / FloatType(1023);
                },
                [](FloatType start, FloatType end, FloatType value)
                {
                    return (std::log(((value - start) * FloatType(1023) / (end - start)) + FloatType(1)) / std::log(FloatType(2))) / FloatType(10);
                },
                [](FloatType start, FloatType end, FloatType value)
                {
                    // optimised for frequencies: >3 kHz: 2 decimals
                    if (value > FloatType(3000))
                        return juce::jlimit(start, end, FloatType(100) * juce::roundToInt(value / FloatType(100)));

                    // optimised for frequencies: 1-3 kHz: 1 decimal
                    if (value > FloatType(1000))
                        return juce::jlimit(start, end, FloatType(10) * juce::roundToInt(value / FloatType(10)));
                    // optimised for frequencies: <3 :
                    if (value < FloatType(3))
                        return juce::jlimit(start, end, juce::roundToInt(value * FloatType(100)) / FloatType(100));
                    // optimised for frequencies: 3-10 : 2 decimals
                    if (value < FloatType(10))
                        return juce::jlimit(start, end, juce::roundToInt(value * FloatType(10)) / FloatType(10));

                        return juce::jlimit(start, end, FloatType(juce::roundToInt(value)));

                });
        }

    }

}

// Macros for FFT size and order. Try changing these!
#define FFT_ORDER 11
#define FFT_SIZE (1 << (FFT_ORDER))

namespace Faust
{
#include "Multiband.h"
    class Multiband;
    class MapUI;
}
//==============================================================================
/**
 */
class FaustAndJuceAudioProcessor : public foleys::MagicProcessor,
                                   private juce::AudioProcessorValueTreeState::Listener
{
public:
    //==============================================================================
    FaustAndJuceAudioProcessor();
    ~FaustAndJuceAudioProcessor() override;

    //==============================================================================
    void prepareToPlay(double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

#ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported(const BusesLayout &layouts) const override;
#endif

    void processBlock(juce::AudioBuffer<float> &, juce::MidiBuffer &) override;

    //==============================================================================
#if 0 // Let FaustAndJuce handle this:
    juce::AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;
#endif
    //==============================================================================
    const juce::String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool isMidiEffect() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram(int index) override;
    const juce::String getProgramName(int index) override;
    void changeProgramName(int index, const juce::String &newName) override;

    //==============================================================================
#if 0 // Let FaustAndJuce handle this:
    void getStateInformation (juce::MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;
#endif
    void parameterChanged(const juce::String &parameterID, float newValue) override;

private:
    //==============================================================================
    juce::AudioProcessorValueTreeState::ParameterLayout createParameterLayout();
    juce::AudioProcessorValueTreeState treeState;
    // Add your member variables here


    // faust process pointer
    std::unique_ptr<Faust::Multiband> fMB;
    // faust UI pointer
    std::unique_ptr<Faust::MapUI> fUI;
    // 2D float pointer for faust IO
    float **faustIn;
    // float **faustO;

    // Visualizer
    // ADD YOUR PARAMETER POINTERS HERE
    double fs;
    float leak;
    float minDbVal = -30;
    float maxDbVal = 30; 
    float centroidVal = 1000;
    
    // PGM SETUP
    foleys::MagicLevelSource *outputMeter = nullptr;
    std::vector<foleys::MagicLevelSource*> grMeters;

    Visualizer *fftPlot = nullptr; // We connect our FFT plot to this
    // PGM Tree state. Needs to be called after createParameterLayour
    // foleys::MagicProcessorState magicState{*this};

    // FFT SETUP
    // Do your FFT setup here...
    // Here's some free buffers
    float fftData[FFT_SIZE / 2];
    float newFftData[FFT_SIZE * 2];
    float oldFftData[FFT_SIZE / 2];
    float frequencies[FFT_SIZE / 2];

    juce::dsp::FFT forwardFFT;                  // [4]
    juce::dsp::WindowingFunction<float> window; // [5]

    float fifo[FFT_SIZE];           // [6]
    int fifoIndex = 0;              // [8]
    bool nextFFTBlockReady = false; // [9]

    // hack eq plot using visualizer
    Visualizer *eqPlot = nullptr;
    // arrays that are passed to eq plot
    float eqData[FFT_SIZE/2];
    float eqFreqs[FFT_SIZE/2];
    //
    float bandNeq[4][FFT_SIZE/2];


    void updateEqPlot();
    void updateBand(int band);


    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(FaustAndJuceAudioProcessor)
};
