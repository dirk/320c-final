/*
  ==============================================================================

    MUSIC 320C - ASSIGNMENT 1 STARTER CODE
    VISUALIZER.CPP
    SPRING 2022

    COPYRIGHT (C) 2022 CENTER FOR COMPUTER RESEARCH IN MUSIC AND ACOUSTICS

  ==============================================================================
*/
#include "Visualizer.h"

// Constructor requires pointer to data array and data array size
Visualizer::Visualizer(float *data, int dataSize) : data(data),
                                                    dataSize(dataSize)
{
}

// POINTER STUFF
// & - address-of
// * - dereference
// POINTERS TO ARRAYS
// arrays already use the pointer system b/c each thing's pointer just points to the next
// thus, we can pass an array's loc by passing a float pointer
// then, we just call the array as normal --- x[i] ---

// Set X write the 'x' pointer. It is up to the user to ensure
// x matches dataSize
void Visualizer::setX(float *newX)
{
  x = newX;
}

// Update calls the parent resetLastDataFlag() method to update the plot
void Visualizer::update()
{
  resetLastDataFlag();
}

void Visualizer::setDbBounds(float newMinDB, float newMaxDB)
{
  minDB = newMinDB;
  maxDB = newMaxDB;
}

// createPlotPaths updates paths
void Visualizer::createPlotPaths(juce::Path &path, juce::Path &filledPath, juce::Rectangle<float> bounds,
                                 foleys::MagicPlotComponent &component)
{
  // not sure what the readlock is for...
  const juce::ScopedReadLock readLock(plotLock);

  // yFactor is plot height / max db value
  const auto yFactor = bounds.getHeight() / (maxDB - minDB);
  // xFactor is plot width / max freq (x)
  const auto xFactor = static_cast<double>(bounds.getWidth()) / (std::log(21000.0f)-std::log(18.0f));

  // ?: --> conditional operators
  // exp 1 ? exp 2 : exp 3
  // if exp1, then exp 2, else exp 3

  path.clear();

  path.startNewSubPath(bounds.getX(), std::log10(data[0]) * 20 > minDB ? bounds.getBottom() - (std::log10(data[0]) * 20 - minDB) * yFactor : bounds.getBottom());
  for (size_t i = 1; i < dataSize; ++i)
    // X point - add log(freq) * xFactor to the left side
    // Y point - subtract log(amplitude) * yfactor from the centerline of the plot
    // why do I need to subtract here??? shouldn't this have the opposite effect of what I want???
    path.lineTo(float(bounds.getX() + (std::log(x[i]) - std::log(18.0f)) * xFactor),
                float(20 * std::log10(data[i]) > minDB ? 
                bounds.getBottom() - (20 * std::log10(std::abs(data[i])) - minDB) * yFactor 
                : bounds.getBottom()));

  // add the path to the plot
  filledPath = path;
  filledPath.lineTo(bounds.getBottomRight());
  filledPath.lineTo(bounds.getBottomLeft()); 
  filledPath.closeSubPath();
}

#if 0
  // Foley's magic plot reference
  void MagicFilterPlot::createPlotPaths(juce::Path &path, juce::Path &filledPath, juce::Rectangle<float> bounds, MagicPlotComponent &)
  {

    const juce::ScopedReadLock readLock(plotLock);

    const auto yFactor = 2.0f * bounds.getHeight() / juce::Decibels::decibelsToGain(maxDB);
    const auto xFactor = static_cast<double>(bounds.getWidth()) / frequencies.size();

    path.clear();
    // first point at x[0], y[0]
    path.startNewSubPath(bounds.getX(), float(magnitudes[0] > 0 ? bounds.getCentreY() - yFactor * std::log(magnitudes[0]) / std::log(2) : bounds.getBottom()));
    for (size_t i = 1; i < frequencies.size(); ++i)
      path.lineTo(float(bounds.getX() + i * xFactor),
                  float(magnitudes[i] > 0 ? bounds.getCentreY() - yFactor * std::log(magnitudes[i]) / std::log(2) : bounds.getBottom()));

    filledPath = path;
    filledPath.lineTo(bounds.getBottomRight());
    filledPath.lineTo(bounds.getBottomLeft());
    filledPath.closeSubPath();
  }
#endif