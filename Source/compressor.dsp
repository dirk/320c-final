import("stdfaust.lib");

// COMPRESSION GAIN 
// split and multiply the input signal to get the compression effect. 
compression_gain(threshold, ratio, attack, release, knee) = level_detector(threshold, attack, release, knee) : ba.linear2db : gain_computer(threshold, ratio, knee) : ba.db2linear
with{
    // LEVEL DETECTOR
    //leaky integrator with attack and release, releases to right before the knee
    // outputs the measured linear level
    level_detector(threshold_ld, attack_ld, release_ld, knee_ld) = leaky_integrator
    with{
        cA = 1 - exp(-1/(ma.SR*attack_ld)); // attack constant
        cR = 1 - exp(-1/(ma.SR*release_ld)); // release constant
        leaky_forward(lambda, x) = lambda + cA*((x:abs) - lambda)*((x:abs)>=lambda) +  cR*(threshold_ld- lambda)*(x:abs<lambda);
        leaky_integrator = leaky_forward ~ _ ;
    };

    // GAIN COMPUTER
    // feedforward gain computer
    // operates in log space -- assumes dB level input, dB GR output, dB threshold, etc. 
    // takes a knee width (dB) smoothing over threshold
    gain_computer(threshold_gc, ratio_gc, knee_width_gc) =  _ <: inKnee(_)*(_ >=( threshold_gc - knee_width_gc))*(_ < ( threshold_gc +knee_width_gc)) 
        + overKnee(_) * (_ > ( threshold_gc +knee_width_gc)) 
    with{
        // make sure ratio is not zero to avoid NaN
        // 
        ratio_check = max(ma.EPSILON,float(ratio)); 
        // make sure knee is not zero to avoid  NaN
        knee_check = max(ma.EPSILON,float(knee)); 
        // gain reduction in the knee (dB)
        inKnee(li) = (1 - 1/ratio_gc)/(2*knee_width_gc)*pow(li - (threshold_gc-knee_width_gc), 2);
        // gain reduction over the knee (dB)
        overKnee(li) = (1/ratio_gc - 1)*(li - threshold_gc);
    };
};

ratio = compGroup(vslider("Ratio[scale:log]", 1, .01, 100, .001));
thresh = compGroup(vslider("Threshold", 0, -80, 0, .1));
attack = compGroup(vslider("Attack[scale:log]", 0.001, 0.0001, 0.1, 0.0001));
release = compGroup(vslider("Release[scale:log]", 0.001, 0.0001, 0.1, 0.0001));
gr = compGroup(vbargraph("GR", -20, 20));
compGroup(x) = hgroup("Compressor", x);

process = _ <: _*(compression_gain(ratio, thresh, attack, release, 5.0):gr);