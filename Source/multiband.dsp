import("stdfaust.lib");


mbGroup(x) = hgroup("MB", x);

band(i) = fi.bandpass(1, fL, fH) : compressor(thresh_p, ratio_p, attack_p, release_p)
with{

    compressor(threshold,ratio,attack,release, x) = (x : compression_gain(threshold, ratio, attack, release, 5.0)) - 1.0 <: _*x, _+1;
    // COMPRESSION GAIN 
    // split and multiply the input signal to get the compression effect. 
    compression_gain(threshold, ratio, attack, release, knee) = level_detector(threshold, attack, release, knee) : ba.linear2db : gain_computer(threshold, ratio, knee) + gain_p: gr : ba.db2linear
    with{
        // LEVEL DETECTOR
        //leaky integrator with attack and release, releases to threshold
        // outputs the measured linear level
        level_detector(threshold_ld, attack_ld, release_ld, knee_ld) = leaky_integrator
        with{
            cA = 1 - exp(-1/(ma.SR*attack_ld)); // attack constant
            cR = 1 - exp(-1/(ma.SR*release_ld)); // release constant
            leaky_forward(lambda, x) = lambda + cA*((x:abs) - lambda)*((x:abs)>=lambda) +  cR*(((threshold_ld - knee_ld/2):ba.db2linear) - lambda)*((x:abs)<lambda);
            leaky_integrator = leaky_forward ~ _ ;
        };

        // GAIN COMPUTER
        // feedforward gain computer
        // operates in log space -- assumes dB level input, dB GR output, dB threshold, etc. 
        // takes a knee width (dB) smoothing over threshold
        gain_computer(threshold_gc, ratio_gc, knee_width_gc) =  _ <: inKnee(_)*(_ >=( threshold_gc - knee_width_gc/2))*(_ < (threshold_gc + knee_width_gc/2)) 
        + overKnee(_) * (_ > ( threshold_gc +knee_width_gc/2)) 
        with{
        // make sure ratio is not zero to avoid NaN
        // 
        ratio_check = max(ma.EPSILON,float(ratio)); 
        // make sure knee is not zero to avoid  NaN
        knee_check = max(ma.EPSILON,float(knee)); 
        // gain reduction in the knee (dB)
        inKnee(li) = -1.0*(1 - 1/ratio_gc)/(2*knee_width_gc)*pow(li - (threshold_gc-knee_width_gc/2), 2);
        // gain reduction over the knee (dB)
        overKnee(li) = (1/ratio_gc - 1)*(li - threshold_gc);
        };
    };
    
    // eq group
    bandGroup(x) = mbGroup(hgroup("Band%i", x));
    eqGroup(x) = bandGroup(x);

    fc = eqGroup(vslider("CenterF[scale:log]", 1000, 20, 20000, .1));
    q =  eqGroup(vslider("Quality[scale:log]", 6, .51, 10, .01));
    gain_p = eqGroup(vslider("Gain", 0, -20, 20, .1)); 

    fH = fc*(1+1/(2*q)), 20000 : min;
    fL = fc*(1-1/(2*q)), 20 : max;

    // compressor group
    compGroup(x) = bandGroup(x);
    gr = bandGroup(vbargraph("GR", -20, 20));
    ratio_p = compGroup(vslider("Ratio[scale:log]", 1, .01, 100, .001));
    thresh_p = compGroup(vslider("Threshold", 0, -80, 0, .1));
    attack_p = compGroup(vslider("Attack[scale:log]", 0.001, 0.0001, 0.1, 0.0001));
    release_p = compGroup(vslider("Release[scale:log]", 0.001, 0.0001, 0.1, 0.0001));
};

process = _  <: par(i, 4, band(i)), _ : route(9, 5, 1, 1, 2, 2, 3, 1, 4, 3, 5, 1, 6, 4, 7, 1, 8, 5, 9, 1) ;