                            <p class="video"> <a href="w6/index.html">week 6</a> </p>
import("stdfaust.lib");

// Mixes between two signals
mixer = (_*(1 - wet) + _*wet)*(0.5);

wet = hslider("dry/wet", 0.5, 0, 1, 0.01);


// "Ringmod" effect. Multiplies an input channel 
// with an amplitude envelope sinewave oscillator
ringmod = _<: _, abs(_)*os.osc(freq), _ : _*_, _ : mixer;

freq = hslider("frequency", 1000, 20, 4000, 1);


// Main process
process = _, _ :> ringmod <: _, _;