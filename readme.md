### MultibandComp - a 320c Final Project

![A multiband compressor plugin](images/plugin.png)

Included here are the source files required to build a multiband compressor I built as my final project for 320c.
Since then, I have continued to improve the plugin, however, it is still in the early stages of development and prototyping. 
Please excuse any visual or audio bugs. 
I hope to continue to update this project moving forward. 

The audio effect itself is a 4-band multiband compressor, written in Faust. 
The input signal is first split into four bands using second order Butterworth bandpass filters. 
Then, each band's signal is compressed using an analog style compressor with a soft knee. 
Finally, the output of each compressor is added back to the original signal. 

The plugin can be used as a pseudo-parametric equalizer by manually changing the overall band gain with the "gain" parameter or band quality with the "quality" parameter. 
The gain computer of the compressor is designed to account for changes made to this parameter, causing an automatic offset to any existing multiband dynamic control. 

The threshold, attack, release, and ratio of the compressor of each band can be changed separately. 
The ratio can be set to high values for a limiting effect or values less than one to act as an expander. 
The soft knee of the compressor seeks to minimize distortion caused by the dynamic processing. 

The visualizer is composed of three elements.  The blue dots that allow for easy adjustment of cuttoff frequency and gain of the each of the four bands. 
The neon green plot is a visualization of the output spectrum computed using the FFT. 
The orange plot is a dynamic visualization of the current overall equalization curve based on the current dynamic processing being applied. 

The plugin itself seeks to replicate the TDR Nova by Tokyo Dawn Labs. 
I often use the TDR nova when mixing and was impressed with its versatility.
I decided to replicate an existing plugin because I wanted to focus my time on learning JUCE and PGM over UI design.  
I chose to replicate the TDR Nova primarily because I wanted to encourage experimentation with complex interface design in JUCE/PGM, namely through the dynamic EQ graph.
Furthermore, I wanted to attempt to replicate the DSP algorithm itself as a way to learn about parallel processing in audio. 

Because this is an attempted direct clone of an existing copyrighted plugin, I have no plans to release this plugin publicly or profit off it in any way.
It was created purely for my individual educational purposes.  

I'm unable to upload any current video footage due to internet upload constraints. I hope to add a video demo of the plugin soon. 

Best,
Dirk Roosenburg

#### Build

I've included a VST3 built on Manjaro Linux in builds. Other users will need to build using the following method:

1. open .JUCER file with JUCE and select your desired export target + platform - note that the Plugin Gui Magic (PGM) module is required for building. 
2. (optional) replace the if statements in foleys_conversions.h with the commented out code at the top of PluginProcessor.cpp - this doesn't effect much, just the granularity of some of the GUI knobs. 
3. Build JUCE project with option CONFIG=release


#### File Description
FaustAndJuce.JUCER - juce header file with configuration and build for my platform (linux)
layout.xml - xml markdown used by PGM to create the GUI layout
multiband.dsp - The uncompiled faust code that defines the multiband audio effect
multiband.h - compiled faust code used by pluginprocessor.h
analyzer.h/.cpp - The analayser I created for the the first project with some minor edits to fix issues with scaling. 
PluginProcessor.h/.cpp - The bulk of the actual plugin's behavior. It uses one instance of the faust plugin that processes the audio and two instances of the analyzer, one to draw the FFT spectrum and one to draw the EQ gain curve.

#### To Implement

- static EQ visualization
- Band bypass
- Frequency and amplitude markings on the plot
- Better gain reduction metering
- general UI improvements
